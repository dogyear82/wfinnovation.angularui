import { browser, by, element } from 'protractor';

export class AppView {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  gettestingConfirmTag(): Promise<string> {
    return element(by.css('p, confirmLoad')).getText() as Promise<string>;
  }
}
