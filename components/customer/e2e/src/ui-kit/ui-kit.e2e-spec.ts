import { UiKitComponent } from './ui-kit.po';
import { browser, logging } from 'protractor';

describe('the ui kit view for e2e test', () => {
  let page: UiKitComponent;

  beforeEach(() => {
    page = new UiKitComponent();
  });

  it('the ui kit view and the auto complete component loaded', () => {
    page.navigateTo();
    expect(page.gettestingConfirmTag()).toEqual('view loaded');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
