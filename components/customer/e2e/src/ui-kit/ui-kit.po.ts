import { browser, by, element } from 'protractor';

export class UiKitComponent {
  loginForm: any;
  // tslint:disable-next-line: typedef
  navigateTo(){
      return browser.get('/ui-kit');
  }

  // tslint:disable-next-line: typedef
  getUserNameTextBox() {
    return element(by.name('myControl'));
   }

  gettestingConfirmTag(): Promise<string> {
    return element(by.css('p, uiKit-ConfirmLoad')).getText() as Promise<string>;
  }
}
