import { browser, by, element } from 'protractor';

export class CustomerComponent {
  loginForm: any;
  // tslint:disable-next-line: typedef
  navigateTo(){
      return browser.get('/customer');
  }

  // tslint:disable-next-line: typedef
  getUserNameTextBox() {
    return element(by.name('firstName'));
   }

  gettestingConfirmTag(): Promise<string> {
    return element(by.css('p, customer-ConfirmLoad')).getText() as Promise<string>;
  }
}
