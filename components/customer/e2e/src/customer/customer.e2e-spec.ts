import { CustomerComponent } from './customer.po';
import { browser, logging } from 'protractor';

describe('customer component view for e2e test', () => {
  let page: CustomerComponent;

  beforeEach(() => {
    page = new CustomerComponent();
  });

  it('the customer view, data and input form loaded', () => {
    page.navigateTo();
    expect(page.gettestingConfirmTag()).toEqual('view loaded');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
