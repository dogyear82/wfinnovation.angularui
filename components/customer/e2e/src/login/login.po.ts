import { browser, by, element } from 'protractor';

export class LoginComponent {
  loginForm: any;
  // tslint:disable-next-line: typedef
  navigateTo(){
      return browser.get('/login');
  }

  // tslint:disable-next-line: typedef
  getUserNameTextBox() {
    return element(by.name('username'));
   }

  gettestingConfirmTag(): Promise<string> {
    return element(by.css('p, login-ConfirmLoad')).getText() as Promise<string>;
  }
}
