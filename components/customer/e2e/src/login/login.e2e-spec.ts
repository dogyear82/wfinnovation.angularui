import { LoginComponent } from './login.po';
import { browser, logging } from 'protractor';

describe('login view for customer component and ui kit e2e test', () => {
  let page: LoginComponent;

  beforeEach(() => {
    page = new LoginComponent();
  });

  it('the login view loaded', () => {
    page.navigateTo();
    expect(page.gettestingConfirmTag()).toEqual('view loaded');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
