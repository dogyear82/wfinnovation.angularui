import { AppView } from './app.po';
import { browser, logging } from 'protractor';

describe('Does the Customer UI  have a heartbeat and load in the browser', () => {
  let page: AppView;

  beforeEach(() => {
    page = new AppView();
  });

  it('the customer UI loaded', () => {
    page.navigateTo();
    expect(page.gettestingConfirmTag()).toEqual('view loaded');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
