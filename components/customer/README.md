# customer component - Angular 10

Angular 10 - Customer Component using JW Tokens for authorization

# Basic CRUD App + Components + JW Token + with Node + Angular

This example app shows how to create a Node.js API and display its data with an Angular UI.

This project was bootstrapped with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

**Prerequisites**: [Node.js](https://nodejs.org/en/).

# Install and Verify NodeJS

In a web browser, navigate to https://nodejs.org/en/download/

1. Once the installer finishes downloading, launch it. Open the downloads link in your browser and click the file. Or, browse to the location where you have saved the file and double-click it to launch.

2. The system will ask if you want to run the software – click Run.

3. You will be welcomed to the Node.js Setup Wizard – click Next.

4. On the next screen, review the license agreement. Click Next if you agree to the terms and install the software.

5. The installer will prompt you for the installation location. Leave the default location, unless you have a specific need to install it somewhere else – then click Next.

6. The wizard will let you select components to include or remove from the installation. Again, unless you have a specific need, accept the defaults by clicking Next.

7. Finally, click the Install button to run the installer. When it finishes, click Finish.

8. Open a command prompt (or PowerShell), and enter the following:

        node –v
        The system should display the Node.js version installed on your system. You can do the same for NPM:
9. In the same command prompt enter the following:
npm –v


# Install Angular and Angular CLI

With the command prompt or powershell open at the directory of your choice enter the following:

npm install -g @angular/cli

This installs the Angular CLI (Command Line Interface) on your system globally (g).

# Choose your Development Environment - This work was done using Visual Studio Code

https://code.visualstudio.com/download (Even those crazy Java developers like VS Code)


## Getting Started with the Actual Application in this repo

Pull the code from the BitBucket or Git Repo as instructed by the team mates.

Put the source files in a "common" area - if the team works in a specific directory, match them on your machine

To install this example application, run the following commands - and this is true for EVERY Angular application - in the "src" directory run:

npm install

Then wait ....

After it is finished run:

npm start

Then wait

After it is finished - it will usually tell you WHERE to open the application. It is usually http://localhost:4200 for angular applications.


# What's Here Again?

-JWToken Examples
-AuthGuard Examples
-Angular Routing Examples
-Models Examples
-Services Examples
-Angular Alert Component Example (shared)
-CRUD Examples for Angular connected to an API
-Material UI Components
-Unit Testing Examples
-E2E Testing Examples
-Bootstrap CSS usage Examples
-General Standard Angular Application Structure
-Dockerfile Examples
-Compodoc Output Example for Tech Documentation


## Links

This example uses the following libraries provided by Okta:

* [Angular Official Site](https://angular.io/)
* [NodeJS Official Site](http://nodejs.org/)
* [JSON Web Tokens (JWT) ](http://jwt.io/)
* [Docker ](http://Docker.com/)


## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).