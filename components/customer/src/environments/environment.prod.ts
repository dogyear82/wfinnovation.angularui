export const environment = {
  production: true,
  apiUrl: 'http://localhost:4000',
  url: 'http://wfinnovation-api.westus2.azurecontainer.io',
  endPointCustomer: 'Customers'
};
