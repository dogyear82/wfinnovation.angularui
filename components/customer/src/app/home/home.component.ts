﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { AlertService } from '../_alert';

@Component({
    selector: 'app-home',
    templateUrl: './home2.component.html',
    // styleUrls: ['./home.component.less']
  })

export class HomeComponent {
    options = {
        autoClose: false,
        keepAfterRouteChange: false
    };
    loading = false;
    users: User[];

    constructor(private userService: UserService, public alertService: AlertService) { }


    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit(): void {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
    }
}
