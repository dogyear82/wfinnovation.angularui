import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CustomerService } from './customer.service';
describe('CustomerService', () => {
  let service: CustomerService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustomerService]
    });
    service = TestBed.get(CustomerService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
