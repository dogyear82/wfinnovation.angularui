import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../_models';
import { BaseService } from './base-service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseService<Customer> {

  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      environment.url,
      environment.endPointCustomer);
  }
}
