import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IUrlOptions } from '../_models/odata';
import { RequestTypes } from '../_models/odata';


// tslint:disable-next-line: typedef
export function provideODataService(url: string) {
  return {
      // tslint:disable-next-line: no-shadowed-variable
      provide: ODataService, useFactory: (HttpClient) => {
          return new ODataService(url, HttpClient);
      },
      deps: [HttpClient]
  };
}

@Injectable({
  providedIn: 'root'
})
export class ODataService {
  sort: any;
  paginator: any;

  constructor(
    private host: string,
    private http: HttpClient
  ) { }

  private constructUrl(urlOptions: IUrlOptions): string {
    return this.host + urlOptions.restOfUrl;
  }

  // T specifies a generic output of function
  public Request<T>(
    requestType: RequestTypes,
    urlOptions: IUrlOptions,
    body?: any,
    options?: any// RequestOptionsArgs
  ): Observable<any> {

    let response: Observable<Response>;

    // True in case of post, put and patch
    if (body && options) {
      response = this.http[RequestTypes[requestType]](
        this.constructUrl(urlOptions),
        body,
        options);
    }
    // True in case of post, put and patch if options is empty
    else if (body) {
      response = this.http[RequestTypes[requestType]](
        this.constructUrl(urlOptions),
        body);
    }
    // True in case of get, delete, head and options
    else if (options) {
      response = this.http[RequestTypes[requestType]](
        this.constructUrl(urlOptions),
        options);
    }
    // True in case of get, delete, head and options, if options is empty
    else {
      response = this.http[RequestTypes[requestType]](
        this.constructUrl(urlOptions),
        options);
    }

    return response
      .pipe(
        map((res) => res)
      );
  }
}
