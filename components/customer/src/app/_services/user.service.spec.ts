import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { UserService } from './user.service';
describe('Fake JWT Auth User Service', () => {
  let service: UserService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    service = TestBed.get(UserService);
  });
  it('Fake JWT auth is working with test user credentials', () => {
    expect(service).toBeTruthy();
  });
  describe('getAll works as expected', () => {
    it('makes expected calls and can flush data', () => {
      const httpTestingController = TestBed.get(HttpTestingController);
      service.getAll().subscribe(res => {
        expect(res).toEqual([{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }]);
      });
      const req = httpTestingController.expectOne('http://localhost:4000/users');
      expect(req.request.method).toEqual('GET');
      req.flush([{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }]);
      httpTestingController.verify();
    });
  });
});
