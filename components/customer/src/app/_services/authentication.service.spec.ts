import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { AuthenticationService } from './authentication.service';
describe('Authentication Service', () => {
  let service: AuthenticationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService]
    });
    service = TestBed.get(AuthenticationService);
  });
  it('the auth service is accessible and does a get with success', () => {
    expect(service).toBeTruthy();
  });
});
