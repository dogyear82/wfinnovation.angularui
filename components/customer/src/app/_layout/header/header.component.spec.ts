import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from '../../app.component';
import { HeaderComponent } from '../header/header.component';
import { FormsModule } from '@angular/forms';

// tslint:disable-next-line: no-submodule-imports
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// tslint:disable-next-line: no-submodule-imports
import { HttpClientModule } from '@angular/common/http';

describe('The header component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, FormsModule],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it('The app loaded the header component (layout)', async(() => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});