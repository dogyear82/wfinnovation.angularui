# _Layout and Header 

The _layout directory contains the header and footer for the UI of the application. The header also has Angular logic to decide whether the current authenticated user is authorized to view specific components within the application.



# Learning

The login checks for authentication, sets the token and then routes the user to the home component. The header component determines the navigation through an NgIf statement on the div tag that pains the navigation and then the auth guard determines if the current user can view the route that coincides with a URL (link). The header component is subscribed to the authentication component and checks it and subsequently the token on each view change. 


## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).