// tslint:disable-next-line: no-submodule-imports
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// tslint:disable-next-line: no-submodule-imports
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// tslint:disable-next-line: no-submodule-imports
import { HttpClientModule } from '@angular/common/http';

describe('Login Component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, ReactiveFormsModule, FormsModule],
      declarations: [
        AppComponent,
        LoginComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it('The app loaded the login component', async(() => {
    const fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
