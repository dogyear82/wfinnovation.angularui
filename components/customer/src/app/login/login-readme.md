# Login view, routing and Json Web Tokens (JWT)

The login view is a standalone component that relies on several different pieces of logic in order to operate. 
- The Authentication Service (_services directory)
- The JWT Interceptor (_helpers directory)
- The Fake Backend (_helpers directory)
- The error interceptor (_helpers directory)
- The auth guard (_helpers directory)

# Learning

- The user is presented the login view from the login component. This form has client side validation and server side validation. On a failed login, the user is presented an error message from the error interceptor. The authentication service is currently conversing with a fake JSON backend to emulate something like Active Directory or a SSO.

- On a successful login, the authentication service says it is acceptable and the system sets a JWToken in the browser for that user. Then the system refers to the auth guard logic to determine which items the user has authorization to view. Then the routes / elements are made available in the application and displayed in the browser. 


[JW Tokens](https://blog.angular-university.io/angular-jwt-authentication/).

## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).