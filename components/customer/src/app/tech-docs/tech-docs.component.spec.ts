import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechDocsComponent } from './tech-docs.component';

describe('TechDocsComponent', () => {
  let component: TechDocsComponent;
  let fixture: ComponentFixture<TechDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
