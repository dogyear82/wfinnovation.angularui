﻿import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AlertModule } from './_alert';

import { fakeBackendProvider } from './_helpers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ErrorInterceptor, JwtInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { CustomerComponent } from './customer/customer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiKitComponent } from './ui-kit/ui-kit.component';
import { MaterialModule } from './material-module';
import { AutoCompleteComponent } from './ui-kit/auto-complete/auto-complete.component';
import { AutoCompleteStatesComponent } from './ui-kit/auto-complete-states/auto-complete-states.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatNativeDateModule} from '@angular/material/core';
import { DatePickerComponent } from './ui-kit/date-picker/date-picker.component';
import { RadioGroupComponent } from './ui-kit/radio-group/radio-group.component';
import { CheckBoxComponent } from './ui-kit/check-box/check-box.component';
import { SelectBoxComponent } from './ui-kit/select-box/select-box.component';
import { WizardStepsComponent } from './ui-kit/wizard-steps/wizard-steps.component';
import { ExpandPanelComponent } from './ui-kit/expand-panel/expand-panel.component';
import { ProgressBarComponent } from './ui-kit/progress-bar/progress-bar.component';
import { LoadingSpinnerComponent } from './ui-kit/loading-spinner/loading-spinner.component';
import { BadgeIndicatorComponent } from './ui-kit/badge-indicator/badge-indicator.component';
import { TabPanelComponent } from './ui-kit/tab-panel/tab-panel.component';
import { DynamicTabPanelComponent } from './ui-kit/dynamic-tab-panel/dynamic-tab-panel.component';
import { DataTableComponent } from './ui-kit/data-table/data-table.component';
import { HeaderComponent } from './_layout/header/header.component';
import { FooterComponent } from './_layout/footer/footer.component';
import { ODataComponent } from './odata/odata.component';
import { provideODataService } from './odata/odata.serviceProvider';
import { FormLayoutComponent } from './ui-kit/form-layout/form-layout.component';
import { DragDropDoubleComponent } from './ui-kit/drag-drop-double/drag-drop-double.component';
import { DragDropSingleComponent } from './ui-kit/drag-drop-single/drag-drop-single.component';
import { TechDocsComponent } from './tech-docs/tech-docs.component';


@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        AlertModule,
        BrowserAnimationsModule,
        MaterialModule,
        MatNativeDateModule
    ],
    declarations: [
        AppComponent,
        CustomerComponent,
        HomeComponent,
        LoginComponent,
        UiKitComponent,
        AutoCompleteComponent,
        AutoCompleteStatesComponent,
        DatePickerComponent,
        RadioGroupComponent,
        CheckBoxComponent,
        SelectBoxComponent,
        WizardStepsComponent,
        ExpandPanelComponent,
        ProgressBarComponent,
        LoadingSpinnerComponent,
        BadgeIndicatorComponent,
        TabPanelComponent,
        DynamicTabPanelComponent,
        DataTableComponent,
        HeaderComponent,
        FooterComponent,
        ODataComponent,
        FormLayoutComponent,
        DragDropDoubleComponent,
        DragDropSingleComponent,
        TechDocsComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
        provideODataService('http://wfinnovation-api.westus2.azurecontainer.io/'),

        // provider used to create fake backend
        fakeBackendProvider,
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }


