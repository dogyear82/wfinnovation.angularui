# Shared Alerts Component and UI Service

This is a customer alert module/component &amp;  service that can be expanded, repurposed and reused in many ways for many things. It should be used to communicate issues or informational messages to the end user when at all possible.

# Learning

Since this alert concept is modular (alert.module.ts) and has it's own dedicated services, it is somewhat portable.  The module container makes the alerts component and service available to the app(s). The real driver of this alert module is the Observable and Injector in the alert service. The observable watches for anything fed to the alert module AND can also be set to watch/listen for the standard browser errors (400s and 500s) and displays messages as the programmer and product owner see fit. 




## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).