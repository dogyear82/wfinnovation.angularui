import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { CustomerComponent } from './customer/customer.component';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { UiKitComponent } from './ui-kit/ui-kit.component';
import { ODataComponent } from './odata/odata.component';
import { FormLayoutComponent } from './ui-kit/form-layout/form-layout.component';
import { TechDocsComponent } from './tech-docs/tech-docs.component';


const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'customer', component: CustomerComponent, canActivate: [AuthGuard] },
    { path: 'ui-kit', component: UiKitComponent, canActivate: [AuthGuard] },
    { path: 'form-layout', component: FormLayoutComponent, canActivate: [AuthGuard] },
    { path: 'odata', component: ODataComponent, canActivate: [AuthGuard] },
    { path: 'techdocs', component: TechDocsComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
