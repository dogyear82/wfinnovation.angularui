import { Component, OnInit } from '@angular/core';
import { Customer } from '../_models';
import { CustomerService } from '../_services/customer.service';
import { NgForm } from '@angular/forms';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  testingConfirmTag = 'view loaded';
  // for alert service
    options = {
      autoClose: false,
      keepAfterRouteChange: false
  };

  customer = {} as Customer;
  customers: Customer[];

  constructor(public alertService: AlertService, private customerService: CustomerService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.customerService.get().subscribe((customers: Customer[]) => {
      this.customers = customers;
      // this.alertService.success('The records were retrieved from the database.', this.options.autoClose);
    });
  }

  save(form: NgForm): void {
    if (this.customer.id !== undefined) {
      this.customerService.update(this.customer).subscribe(() => {
        this.cleanForm(form);
        this.alertService.success('Your change was saved in the database.', this.options.autoClose);
      });
    } else {
      this.customerService.create(this.customer).subscribe(() => {
        this.cleanForm(form);
        this.alertService.success('Your change was saved in the database.', this.options.autoClose);
      });
    }
  }

  edit(customer: Customer): void {
    this.customer = { ...customer };
  }

  // tslint:disable-next-line:no-shadowed-variable
  delete(customer: Customer): void {
    this.customerService.delete(customer).subscribe(() => {
      this.getAll();
    });
  }

  cleanForm(form: NgForm): void {
    this.getAll();
    form.resetForm();
    this.customer = {} as Customer;
  }
}
