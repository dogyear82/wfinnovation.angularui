// tslint:disable-next-line: no-submodule-imports
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from '../app.component';
import { CustomerComponent } from '../customer/customer.component';
import { FormsModule } from '@angular/forms';

// tslint:disable-next-line: no-submodule-imports
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// tslint:disable-next-line: no-submodule-imports
import { HttpClientModule } from '@angular/common/http';

describe('CustomerComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, FormsModule],
      declarations: [
        AppComponent,
        CustomerComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it('The app loaded the customer component', async(() => {
    const fixture = TestBed.createComponent(CustomerComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
