import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicTabPanelComponent } from './dynamic-tab-panel.component';

describe('Dynamic TabPanel Component', () => {
  let component: DynamicTabPanelComponent;
  let fixture: ComponentFixture<DynamicTabPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicTabPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicTabPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Dynamic Tab Panel Loaded', () => {
    expect(component).toBeTruthy();
  });
});
