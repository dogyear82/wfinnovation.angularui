import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-dynamic-tab-panel',
  templateUrl: './dynamic-tab-panel.component.html',
  styleUrls: ['./dynamic-tab-panel.component.less']
})
export class DynamicTabPanelComponent {
  tabs = ['First', 'Second', 'Third'];
  selected = new FormControl(0);

  // tslint:disable-next-line: typedef
  addTab(selectAfterAdding: boolean) {
    this.tabs.push('New');

    if (selectAfterAdding) {
      this.selected.setValue(this.tabs.length - 1);
    }
  }

  // tslint:disable-next-line: typedef
  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }

}
