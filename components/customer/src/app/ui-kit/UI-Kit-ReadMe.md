# UI KIT component - and child components

The UI Kit Component is a parent component that displays it's children in a nicely containerized fashion. 

# Learning

Open the file UI-Kit.component.html -- 

There you will see the special "selectors" that are created by Angular - a selector is something that looks like an HTML tag, but it is actually a call to another Angular component. 
`<app-auto-complete></app-auto-complete>` is an example of a selector. It corresponds with the child component that is stored in the auto-complete directory.

These child components use an Angular library called Material. These can be styled however we like with relative easy. The Material components are a nice standard and they have their own "selectors" too. 

[Angular Material](https://material.angular.io/).

## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).