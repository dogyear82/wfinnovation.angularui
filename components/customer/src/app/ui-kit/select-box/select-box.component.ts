import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-box',
  templateUrl: './select-box.component.html',
  styleUrls: ['./select-box.component.less']
})
export class SelectBoxComponent {
    selected = 'option2';
}
