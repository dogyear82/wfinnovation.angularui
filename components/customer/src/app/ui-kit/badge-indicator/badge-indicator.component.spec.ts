import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeIndicatorComponent } from './badge-indicator.component';

describe('Badge Indicator Component', () => {
  let component: BadgeIndicatorComponent;
  let fixture: ComponentFixture<BadgeIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('The badge compnent loaded successfully', () => {
    expect(component).toBeTruthy();
  });
});
