import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge-indicator',
  templateUrl: './badge-indicator.component.html',
  styleUrls: ['./badge-indicator.component.less']
})
export class BadgeIndicatorComponent {
  hidden = false;

  // tslint:disable-next-line: typedef
  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }
}
