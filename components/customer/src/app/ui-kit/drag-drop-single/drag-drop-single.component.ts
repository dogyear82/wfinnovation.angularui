import { Component } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-drag-drop-single',
  templateUrl: './drag-drop-single.component.html',
  styleUrls: ['./drag-drop-single.component.less']
})
export class DragDropSingleComponent {

  movies = [
    'First Item',
    'Fourth Item',
    'Fifth Item',
    'Seventh Item',
    'Second Item',
    'Sixth Item',
    'Third Item',
    'Eighth Item',
    'Ninth Item'
  ];

  // tslint:disable-next-line: typedef
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.movies, event.previousIndex, event.currentIndex);
  }

}
