import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropSingleComponent } from './drag-drop-single.component';

describe('DragDropSingleComponent', () => {
  let component: DragDropSingleComponent;
  let fixture: ComponentFixture<DragDropSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragDropSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
