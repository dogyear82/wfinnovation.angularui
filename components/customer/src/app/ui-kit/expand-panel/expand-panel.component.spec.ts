import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandPanelComponent } from './expand-panel.component';

describe('Expand Panel Component', () => {
  let component: ExpandPanelComponent;
  let fixture: ComponentFixture<ExpandPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('The expand panel (show hide) loaded', () => {
    expect(component).toBeTruthy();
  });
});
