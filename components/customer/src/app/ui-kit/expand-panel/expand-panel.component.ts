import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expand-panel',
  templateUrl: './expand-panel.component.html',
  styleUrls: ['./expand-panel.component.less']
})
export class ExpandPanelComponent{
  panelOpenState = false;
}
