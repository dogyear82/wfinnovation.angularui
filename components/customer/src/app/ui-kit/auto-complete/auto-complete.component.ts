import {Component} from '@angular/core';
import {FormControl} from '@angular/forms';

/**
 * @title Simple autocomplete
 */
@Component({
  selector: 'app-auto-complete',
  templateUrl: 'auto-complete.component.html',
  styleUrls: ['auto-complete.component.less'],
})
export class AutoCompleteComponent {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
}
