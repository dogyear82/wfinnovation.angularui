import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiKitComponent } from './ui-kit.component';

describe('Ui Kit Component', () => {
  let component: UiKitComponent;
  let fixture: ComponentFixture<UiKitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiKitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiKitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('The UI Kit View Loaded and all the components in it loaded too!', () => {
    expect(component).toBeTruthy();
  });
});
