import { Component, OnInit } from '@angular/core';

import { ODataService } from '../_services/odata.service';
import { RequestTypes } from '../_models/odata';
import { IUrlOptions } from '../_models/odata';

@Component({
    selector: 'app-odata',
    templateUrl: 'odata.component.html'
})
export class ODataComponent implements OnInit {


    public requestResult: any;

    constructor(
        private odata: ODataService
    ) { }

    ngOnInit(): void { }

    testGet(): void {
        const urlOptions: IUrlOptions = {} as IUrlOptions;
        urlOptions.restOfUrl = 'Customers?$format=json';
        this.odata.Request(RequestTypes.get, urlOptions).subscribe(
            data => this.requestResult = data,
            error => alert(error)
        );
    }

    testPost(): void {
        const urlOptions: IUrlOptions = {} as IUrlOptions;
        urlOptions.restOfUrl = 'Customers?$format=json';
        this.odata.Request(RequestTypes.post, urlOptions).subscribe(
            data => this.requestResult = data,
            error => alert(error)
        );
    }

    testPut(): void {
        const urlOptions: IUrlOptions = {} as IUrlOptions;
        urlOptions.restOfUrl = 'Customers?$format=json';
        this.odata.Request(RequestTypes.put, urlOptions).subscribe(
            data => this.requestResult = data,
            error => alert(error)
        );
    }

    testPatch(): void {
        const urlOptions: IUrlOptions = {} as IUrlOptions;
        urlOptions.restOfUrl = 'Customers?$format=json';
        this.odata.Request(RequestTypes.patch, urlOptions).subscribe(
            data => this.requestResult = data,
            error => alert(error)
        );
    }

    testDelete(): void {
        const urlOptions: IUrlOptions = {} as IUrlOptions;
        urlOptions.restOfUrl = 'Customers?$format=json';
        this.requestResult = this.odata.Request(RequestTypes.delete, urlOptions);
    }

}
