# OData component 

This is very much a work in progress

# Learning

The OData API is a REST API standard and should make the way that we share our APIs out to other groups and across applications much more consistent. 

[OData](https://www.odata.org/).

## Help

bryan.howell@wellsfargo.com
tan.nguyen2@wellsfargo.com

## License

Apache 2.0, see [LICENSE](LICENSE).