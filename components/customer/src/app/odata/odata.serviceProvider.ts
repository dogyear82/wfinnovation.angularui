import { HttpClient } from '@angular/common/http';

import { ODataService } from '../_services/odata.service';

// tslint:disable-next-line: typedef
export function provideODataService(url: string) {
    return {
        // tslint:disable-next-line: no-shadowed-variable
        provide: ODataService, useFactory: (HttpClient) => {
            return new ODataService(url, HttpClient);
        },
        deps: [HttpClient]
    };
}
