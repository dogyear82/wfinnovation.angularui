import { viewerType } from './viewerType';

export interface Viewer {
    name: viewerType;
    docs: string[];
    custom: boolean;
    acceptedUploadTypes: string;
    viewerUrl?: string;
  }
