import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageViewerComponent } from './image-viewer.component';
import { RouterModule } from '@angular/router';
import { ImageViewerRoutingModule } from './image-viewer-routing.module';
import { ImageService } from '../services/image-viewer.service';
import { ImgFallbackModule } from 'ngx-img-fallback';


@NgModule({
  declarations: [ImageViewerComponent],
  imports: [
    RouterModule.forChild(ImageViewerRoutingModule),
    CommonModule,
    ImgFallbackModule,
  ],
  providers: [ImageService],
})
export class ImageViewerModule {}
