import { Component, OnInit, SecurityContext } from '@angular/core';
import { ImageService } from '../services/image-viewer.service';
import { ɵDomSanitizerImpl, DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.css']
})
export class ImageViewerComponent implements OnInit{

  workingPlaceholder = '../assets/images/loader.gif';

  imgUrl = 'https://picsum.photos/200/300/?random';

  safeUrl = this._sanitizer.bypassSecurityTrustResourceUrl(this.imgUrl);
  sanitizedUrl = this._sanitizerImpl.sanitize(SecurityContext.RESOURCE_URL, this.safeUrl);

  imageToShow: any;
  isImageLoading: boolean;

  // tslint:disable-next-line: variable-name
  constructor(protected _sanitizer: DomSanitizer, protected _sanitizerImpl: ɵDomSanitizerImpl, private imageService: ImageService) {}

  createImageFromBlob(image: Blob): void {
   const reader = new FileReader();
   reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
   }, false);

   if (image) {
      reader.readAsDataURL(image);
   }
  }

  getImageFromService(): void {
      this.isImageLoading = true;
      this.imageService.getImage(this.sanitizedUrl).subscribe(data => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }
  ngOnInit(): void {
    this.getImageFromService();
  }

}
