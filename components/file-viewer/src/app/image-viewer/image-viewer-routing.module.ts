// src/app/components/details/details.routes.ts
import { Route } from '@angular/router';
import { ImageViewerComponent } from './image-viewer.component';

export const ImageViewerRoutingModule: Route[] = [
  {
    path: '',
    component: ImageViewerComponent,
  },
];
