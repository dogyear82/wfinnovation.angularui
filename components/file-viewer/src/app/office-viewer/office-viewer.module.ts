import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfficeViewerComponent } from './office-viewer.component';



@NgModule({
  declarations: [OfficeViewerComponent],
  imports: [
    CommonModule
  ]
})
export class OfficeViewerModule { }
