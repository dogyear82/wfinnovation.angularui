import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


const routes: Routes = [
  
  {
    path: 'image-viewer',
    loadChildren: () => import('./image-viewer/image-viewer.module')
      .then(mod => mod.ImageViewerModule)
  },
  {
    path: 'pdf-viewer',
    loadChildren: () => import('./pdf-viewer/pdf-viewer.module')
      .then(mod => mod.PdfViewerModule)
  },
  {
    path: 'office-viewer',
    loadChildren: () => import('./office-viewer/office-viewer.module')
      .then(mod => mod.OfficeViewerModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
