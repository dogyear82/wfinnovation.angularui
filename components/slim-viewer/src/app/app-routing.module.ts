import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SimpleComponent } from './simple/simple.component';
import { Base64Component } from './base64/base64.component';
import { ImgBase64Component } from './imgBase64/img-base64.component';


const routes: Routes = [
  { path: 'simple', component: SimpleComponent},
  { path: 'base64', component: Base64Component},
  { path: 'imgbase64', component: ImgBase64Component},
  // otherwise redirect to simple
  { path: '**', redirectTo: 'simple' }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
