export const environment = {
  production: true,
  serviceUrl: 'https://services.odata.org/TripPinRESTierService',
  sampleServiceUrl: 'http://wfinnovation-api.westus2.azurecontainer.io'
};
