export interface ICustomer {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    address?: IAddress;
    loans?: [];
  }

export interface IAddress {
    Address?: string;
    City?: ICity;
  }
export interface ICity {
    Name: string;
    CountryRegion: string;
    Region: string;
  }
