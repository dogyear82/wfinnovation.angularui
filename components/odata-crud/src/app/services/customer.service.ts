import { Injectable } from '@angular/core';
import { ICustomer } from './ICustomer';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ODataStore, IStoreSettings } from '@lucasheight/odata-observable-store';

@Injectable({
  providedIn: 'root',
})
export class CustomerService extends ODataStore<ICustomer> {
  baseUrl = 'Customers';

  constructor(protected http: HttpClient) {
    super(http, { notifyOnGet: true, use$countOnQuery: false });
  }
  // demonstrates how to apply a filter to a query
  queryByfirstName = (query: string): void => {
    const filter = `$filter=contains(firstName,'${query}')`;
    this.query(filter);
  }
  count = (): Observable<number> =>
    this.http.get<number>(`${this.baseUrl}/$count`)
}
