import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { CustomerService } from '../services/customer.service';
import { Subject, Observable, fromEvent } from 'rxjs';
import { ICustomer } from '../services/ICustomer';
import {
  takeUntil,
  map,
  debounceTime,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { IStoreNotifier } from '@lucasheight/odata-observable-store';

// import { action, IStoreNotifier } from '@lucasheight/odata-observable-store'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  providers: [CustomerService],
  styles: [
    'div.wrapper{width:100%;padding:1em}',
    'div.Customer{border:1px solid lightgrey; width:45%;float:left;margin:4px;padding:5px}',
    'div.Customer form label{display:inline-block;width:100px; text-align:right}',
    'div.Customer form div {margin-bottom:5px}',
    'div.buttons{margin-top:2em;text-align:center}',
  ],
})
export class CustomerComponent implements OnInit, OnDestroy, AfterViewInit {
  destroy$: Subject<void> = new Subject();
  customers$: Observable<ICustomer[]>;
  customer$: Observable<ICustomer>;
  message$: Observable<IStoreNotifier<ICustomer>>;
  formGroup: FormGroup = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', Validators.required],
    phone: [''],
  });
  isNew = true;
  @ViewChild('search') searchCtr: ElementRef;

  constructor(
    private customerService: CustomerService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.resetForm();
    this.message$ = this.customerService.notifier$.pipe(
      takeUntil(this.destroy$)
    );

    this.message$
      .pipe(
        takeUntil(this.destroy$),
        filter((f) => f.action === 'Delete')
      )
      .subscribe(() => {
        // clear the edit form after a delete
        this.resetForm();
      });

    this.customers$ = this.customerService.state$.pipe(
      takeUntil(this.destroy$),
      map((m) => m.value)
    );
    this.customerService.query();
  }
  ngOnDestroy(): void {
    this.destroy$.next();
  }
  ngAfterViewInit(): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    fromEvent<any>(this.searchCtr.nativeElement, 'input')
      .pipe(takeUntil(this.destroy$), debounceTime(300), distinctUntilChanged())
      .subscribe((s) => {
        const val: string = s.target.value;
        if (val.length > 0) {
          this.customerService.queryByfirstName(val);
          // could have done this as well:
          // this.customerService.query(`$filter=contains(firstName, '${val}')`);
        } else {
          this.customerService.query();
        }
      });
  }
  public onCustomer = (firstName: string): void => {
    this.customerService
      .get({ firstName } as ICustomer, 'firstName')
      .subscribe((s) => {
        this.isNew = false;
        this.formGroup.setValue({
          firstName: s.firstName,
          lastName: s.lastName,
          email: s.email,
          phone: s.phone,
        });
        this.formGroup.get('firstName').disable();
      });
  }
  public onSave = (): void => {
    const item: ICustomer = this.formGroup.value;
    item.firstName = this.formGroup.getRawValue().firstName;
    console.log(this.isNew ? 'Insert' : 'Update', item);
    this.isNew
      ? this.customerService.insert(item)
      : this.customerService.patch(item, 'firstName');
  }
  public onRemove = (firstName: string): void => {
    console.log('remove', firstName);
    // tslint:disable-next-line:object-literal-shorthand
    this.customerService.remove({ firstName } as ICustomer, 'firstName');
  }
  public onCancel = (): void => this.resetForm();

  private resetForm(): void {
    this.isNew = true;
    this.formGroup.setValue({
      FirstName: '',
      LastName: '',
      Email: '',
      Phone: '',
    });
    this.formGroup.get('firstName').enable();
  }
}
